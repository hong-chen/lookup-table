import os
import sys
import glob
import datetime
import multiprocessing as mp
import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams




def CAL_LAYER_PROPERTY(f_up, f_down, topN=-1, bottomN=0, scaleN=1.0, tag='all'):

    transmittance  = f_down[bottomN, ...]/f_down[topN, ...] * scaleN
    albedo_bottom  = f_up[bottomN, ...]/f_down[bottomN, ...] * scaleN
    albedo_top     = f_up[topN, ...]/f_down[topN, ...] * scaleN

    f_net_top      = f_down[topN, ...] - f_up[topN, ...]
    f_net_bottom   = f_down[bottomN, ...] - f_up[bottomN, ...]

    absorptance    = (f_net_top-f_net_bottom)/f_down[topN, ...] * scaleN
    reflectance    = (f_up[topN, ...] - f_up[bottomN, ...]) / f_down[topN, ...] * scaleN
    if tag.lower() == 'transmittance':
        return transmittance
    elif tag.lower() == 'reflectance':
        return reflectance
    elif tag.lower() == 'absorptance':
        return absorptance
    elif tag.lower() == 'albedo-top':
        return albedo_top
    elif tag.lower() == 'albedo-bottom':
        return albedo_bottom
    elif tag.lower() == 'all':
        return [transmittance, reflectance, absorptance, albedo_top, albedo_bottom]





def GEN_LUT(date=datetime.date.today(),                            # date (datetime.date)
            wavelength_pair=(870.0, 2130.0),                       # two wavelengths (list or tuple)
            surface_albedo_pair=(0.03, 0.03),                      # two surface albedos (list or tuple)
            solar_zenith_angle=0.0,                                # solar zenith angle (float)
            cloud_optical_thickness_all=np.arange(0.0, 50.1, 2.0), # cloud optical thickness array (numpy.ndarray)
            cloud_effective_radius_all=np.arange(4.0, 25.1, 1.0),  # cloud effective radius (numpy.ndarray)
            output_altitude=np.array([0.8, 2.0]),                  # output altitude for libRadtran calculations
            fdir_tmp='data/tmp',                                   # directory to store temporary data (string)
            fdir_lut='data',                                       # directory to store lookup table data
            prop_tag='albedo-top'):                                # property tag, can be "albedo-top", "albedo-bottom", "reflectance", "transmittance", "absorptance" (string)

    sys.path.append(os.path.abspath('libRadtran-util'))

    from lrt_util import cld_cfg, aer_cfg, lrt_cfg
    from lrt_util import LRT_V2_INIT, LRT_RUN_MP, LRT_READ

    fdir_tmp = os.path.abspath(fdir_tmp)
    fdir_lut = os.path.abspath(fdir_lut)
    date_s   = datetime.datetime.strftime(date, '%Y%m%d')

    wvl_x, wvl_y = wavelength_pair
    alb_x, alb_y = surface_albedo_pair
    sza          = solar_zenith_angle

    if not os.path.exists(fdir_tmp):
        os.system('mkdir -p %s' % fdir_tmp)

    if len(glob.glob('%s/*.txt' % fdir_tmp)) > 0:
        os.system('find %s -name "*.txt" | xargs rm -f' % fdir_tmp)

    inits_x = []
    inits_y = []

    cld_cfg['wc_properties'] = 'mie'

    for cot in cloud_optical_thickness_all:
        for cer in cloud_effective_radius_all:

            cld_cfg['cloud_file']              = '%s/LRT_cloud_%04.1f_%04.1f.txt' % (fdir_tmp, cot, cer)
            cld_cfg['cloud_optical_thickness'] = cot
            cld_cfg['cloud_effective_radius']  = cer

            input_file_x  = '%s/LRT_input_%4.4dnm_%04.1f_%04.1f_%04.1f_%04.2f.txt'  % (fdir_tmp, wvl_x, cot, cer, sza, alb_x)
            output_file_x = '%s/LRT_output_%4.4dnm_%04.1f_%04.1f_%04.1f_%04.2f.txt' % (fdir_tmp, wvl_x, cot, cer, sza, alb_x)

            input_file_y  = '%s/LRT_input_%4.4dnm_%04.1f_%04.1f_%04.1f_%04.1f.txt'  % (fdir_tmp, wvl_y, cot, cer, sza, alb_y)
            output_file_y = '%s/LRT_output_%4.4dnm_%04.1f_%04.1f_%04.1f_%04.1f.txt' % (fdir_tmp, wvl_y, cot, cer, sza, alb_y)

            init_x = LRT_V2_INIT(output_altitude=output_altitude, input_file=input_file_x, output_file=output_file_x, date=date, surface_albedo=alb_x, wavelength=wvl_x, solar_zenith_angle=sza, cld_cfg=cld_cfg)
            init_y = LRT_V2_INIT(output_altitude=output_altitude, input_file=input_file_y, output_file=output_file_y, date=date, surface_albedo=alb_y, wavelength=wvl_y, solar_zenith_angle=sza, cld_cfg=cld_cfg)

            inits_x.append(init_x)
            inits_y.append(init_y)

    LRT_RUN_MP(inits_x+inits_y)

    lrt_x = LRT_READ(inits_x)
    lrt_y = LRT_READ(inits_y)

    prop_x = CAL_LAYER_PROPERTY(lrt_x.f_up, lrt_x.f_down, tag=prop_tag).reshape((cloud_optical_thickness_all.size, cloud_effective_radius_all.size))
    prop_y = CAL_LAYER_PROPERTY(lrt_y.f_up, lrt_y.f_down, tag=prop_tag).reshape((cloud_optical_thickness_all.size, cloud_effective_radius_all.size))

    if not os.path.exists(fdir_lut):
        os.system('mkdir -p %s' % fdir_lut)

    fname = '%s/LUT_%s_%s_%4.4dnm-%4.4dnm_%04.2f-%04.2f_%04.1f.h5' % (fdir_lut, date_s, prop_tag, wvl_x, wvl_y, alb_x, alb_y, sza)
    f = h5py.File(fname, 'w')
    f['prop_x'] = prop_x
    f['prop_y'] = prop_y
    f['cot']    = cloud_optical_thickness_all
    f['cer']    = cloud_effective_radius_all
    f.close()
    PLT_LUT(fname)





def PLT_LUT(fname, prop_x0=-1.0, prop_y0=-1.0, fdir_out='data'):

    filename = fname.split('/')[-1][:-3]
    words    = filename.split('_')
    prop     = words[2].replace('-', ' ').title()
    wvl_pair = []
    for word in words:
        if 'nm' in word:
            wavelengths = word.split('-')
            for wavelength in wavelengths:
                if wavelength[0] == '0':
                    wavelength = wavelength[1:]
                wvl_pair.append(wavelength)

    f = h5py.File(fname, 'r')
    cer = f['cer'][...]
    cot = f['cot'][...]
    prop_x = f['prop_x'][...]
    prop_y = f['prop_y'][...]
    f.close()

    rcParams['font.size'] = 15
    fig = plt.figure(figsize=(6.5, 6.0))
    ax1 = fig.add_subplot(111)

    for i in range(cer.size):
        ax1.plot(prop_x[:, i], prop_y[:, i], color='r', zorder=0, lw=0.5, alpha=0.2)
        ax1.text(prop_x[-1, i]+0.008, prop_y[-1, i], '%.1f' % cer[i], fontsize=6, color='r', va='center', zorder=1, weight=0)

    for i in range(cot.size):
        ax1.plot(prop_x[i, :], prop_y[i, :], color='b', lw=0.5, zorder=1, alpha=0.2)
        ax1.text(prop_x[i, -1], prop_y[i, -1]-0.02, '%.1f' % cot[i], fontsize=6, color='b', ha='center', zorder=1, weight=0)

    ax1.scatter(prop_x0, prop_y0, c='k', s=1)
    ax1.set_xlim([-0.05, 1.05])
    ax1.set_ylim([-0.05, 1.05])
    ax1.set_xlabel('%s [%s]' % (prop, wvl_pair[0]))
    ax1.set_ylabel('%s [%s]' % (prop, wvl_pair[1]))
    plt.savefig('%s/%s.svg' % (fdir_out, filename))
    plt.close(fig)





if __name__ == '__main__':

    # kwargs={'date'                       : datetime.date(2014, 9, 1),
    #         'wavelength_pair'            : (1240.0, 2130.0),
    #         'surface_albedo_pair'        : (0.45, 0.05),
    #         'solar_zenith_angle'         : 70.0,
    #         'cloud_optical_thickness_all': np.arange(0.0, 30.1, 2.0),
    #         'cloud_effective_radius_all' : np.arange(5.0, 25.1, 1.0),
    #         'output_altitude'            : np.array([0.8, 1.5]),
    #         'fdir_tmp'                   : 'data/tmp',
    #         'fdir_lut'                   : 'data',
    #         'prop_tag'                   : 'reflectance'}

    # kwargs={'date'                       : datetime.date(2014, 9, 1),
    #         'wavelength_pair'            : (1630.0, 2130.0),
    #         'surface_albedo_pair'        : (0.12, 0.05),
    #         'solar_zenith_angle'         : 70.0,
    #         'cloud_optical_thickness_all': np.arange(0.0, 30.1, 2.0),
    #         'cloud_effective_radius_all' : np.arange(5.0, 25.1, 1.0),
    #         'output_altitude'            : np.array([0.8, 1.5]),
    #         'fdir_tmp'                   : 'data/tmp',
    #         'fdir_lut'                   : 'data',
    #         'prop_tag'                   : 'reflectance'}

    kwargs={'date'                       : datetime.date(2016, 9, 20),
            'wavelength_pair'            : (860, 1630.0),
            'surface_albedo_pair'        : (0.03, 0.03),
            'solar_zenith_angle'         : 0.0,
            'cloud_optical_thickness_all': np.arange(0.0, 30.1, 2.0),
            'cloud_effective_radius_all' : np.arange(5.0, 25.1, 1.0),
            'output_altitude'            : np.array([0.8, 1.5]),
            'fdir_tmp'                   : 'data/tmp',
            'fdir_lut'                   : 'data',
            'prop_tag'                   : 'reflectance'}
    GEN_LUT(**kwargs)
